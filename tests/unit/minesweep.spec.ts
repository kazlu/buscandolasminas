import {
  FREE, BOMB, EXPLODED, play, done, generateRandomGame,
} from '@/services/minesweep';

describe.each([
  [
    "lose first",
    0, 0,
    [
      [BOMB, FREE, FREE],
      [FREE, FREE, FREE],
      [FREE, FREE, FREE],
    ],
    [
      [EXPLODED, FREE, FREE],
      [FREE, FREE, FREE],
      [FREE, FREE, FREE],
    ]
  ],

  [
    "alive mark close",
    0, 1,
    [
      [BOMB, FREE, FREE],
      [FREE, FREE, FREE],
      [FREE, FREE, FREE],
    ],
    [
      [BOMB, 1, FREE],
      [FREE, FREE, FREE],
      [FREE, FREE, FREE],
    ]
  ],

  [
    "alive 2 neighbors",
    0, 1,
    [
      [BOMB, FREE, FREE],
      [BOMB, FREE, FREE],
      [FREE, FREE, FREE],
    ],
    [
      [BOMB, 2, FREE],
      [BOMB, FREE, FREE],
      [FREE, FREE, FREE],
    ]
  ],

  [
    "uncover more than 1",
    2, 2,
    [
      [BOMB, FREE, FREE],
      [BOMB, FREE, FREE],
      [FREE, FREE, FREE],
    ],
    [
      [BOMB, 2, 0],
      [BOMB, 2, 0],
      [FREE, 1, 0],
    ]
  ],

  [
    "uncover all",
    2, 0,
    [
      [BOMB, FREE, FREE],
      [FREE, FREE, FREE],
      [FREE, FREE, FREE],
    ],
    [
      [BOMB, 1, 0],
      [1, 1, 0],
      [0, 0, 0],
    ]
  ],

  [
    "larger game",
    2, 1,
    [
      [BOMB, FREE, FREE, FREE],
      [FREE, FREE, FREE, FREE],
      [FREE, FREE, FREE, FREE],
      [FREE, FREE, FREE, BOMB],
    ],
    [
      [BOMB, 1, 0, 0],
      [1, 1, 0, 0],
      [0, 0, 1, 1],
      [0, 0, 1, BOMB],
    ]
  ],

  [
    "larger game v2",
    1, 1,
    [
      [BOMB, FREE, FREE, FREE],
      [FREE, FREE, FREE, FREE],
      [FREE, FREE, FREE, FREE],
      [FREE, FREE, FREE, BOMB],
    ],
    [
      [BOMB, FREE, FREE, FREE],
      [FREE, 1, FREE, FREE],
      [FREE, FREE, FREE, FREE],
      [FREE, FREE, FREE, BOMB],
    ]
  ],
])('play game', (name, x, y, game, expectedGame) => {
  test(name, () => {
    // Arrange
    // Act
    const newGame = play(game, x, y);

    // Assert
    expect(newGame).toEqual(expectedGame);
  });
});

describe('game done', () => {
  test('not done', () => {
    // Arrange
    const game = [
      [BOMB, FREE, FREE, FREE],
      [FREE, FREE, FREE, FREE],
      [FREE, FREE, FREE, FREE],
      [FREE, FREE, FREE, BOMB],
    ];

    // Act Assert
    expect(done(game)).toEqual(false);
  });

  test('done win', () => {
    // Arrange
    const game = [
      [BOMB, 1, 0, 0],
      [1, 1, 0, 0],
      [0, 0, 1, 1],
      [0, 0, 1, BOMB],
    ];

    // Act Assert
    expect(done(game)).toEqual(true);
  });

  test('not done', () => {
    // Arrange
    const game = [
      [BOMB, 1, 0, 0],
      [FREE, FREE, 0, 0],
      [0, 0, 1, 1],
      [0, 0, 1, BOMB],
    ];

    // Act Assert
    expect(done(game)).toEqual(false);
  });

  test('done lose', () => {
    // Arrange
    const game = [
      [EXPLODED, 1, 0, 0],
      [FREE, 1, 0, 0],
      [0, 0, 1, 1],
      [0, 0, 1, BOMB],
    ];

    // Act Assert
    expect(done(game)).toEqual(true);
  });
});

describe.each([
  [0, 10],
  [3, 10],
  [7, 15],
])(
  "generate random game",
  (nBombs, length) => {
    test("3 bombs", () => {

      // Act
      const game = generateRandomGame(length, nBombs);

      // Assert
      expect(game.length).toEqual(length);
      expect(game[0].length).toEqual(length);
      let actualBombs = countBombs(game);
      expect(actualBombs).toEqual(nBombs);
    });
  });

function countBombs(game: number[][]): number {
  let nBombs = 0;
  for (let i = 0; i < game.length; i += 1) {
    for (let j = 0; j < game.length; j += 1) {
      if (game[i][j] === BOMB) {
        nBombs += 1;
      }
    }
  }

  return nBombs;
}
