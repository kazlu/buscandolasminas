// Negative number for unplayed cells
export const FREE = -1;
export const BOMB = -2;

// Positive for played. Max number of mines is 9
export const EXPLODED = 10;

const DX = [-1, -1, -1, 0, 0, 1, 1, 1];
const DY = [-1, 0, 1, -1, 1, -1, 0, 1];


function bounds(x: number, y: number, l: number): boolean {
  return x >= 0 && x < l
    && y >= 0 && y < l;
}

function neighborCount(game: number[][], x: number, y: number): number {
  let ans = 0;

  for (let d = 0; d < DX.length; d += 1) {
    const nx = x + DX[d];
    const ny = y + DY[d];

    if (bounds(nx, ny, game.length) && game[nx][ny] === BOMB) {
      ans += 1;
    }
  }

  return ans;
}

export function play(game: number[][], x: number, y: number): number[][] {
  // Hopefully this copies it
  let newGame = game.map(arr => arr.slice());

  // Already played
  if (newGame[x][y] >= 0) {
    return newGame;
  }

  if (newGame[x][y] === BOMB) {
    newGame[x][y] = EXPLODED;
    return newGame;
  }

  newGame[x][y] = neighborCount(game, x, y);
  if (newGame[x][y] === 0) {
    for (let d = 0; d < DX.length; d += 1) {
      const nx = x + DX[d];
      const ny = y + DY[d];

      if (bounds(nx, ny, newGame.length)) {
        newGame = play(newGame, nx, ny);
      }
    }
  }

  return newGame;
}

export function done(game: number[][]): boolean {
  let nFree = 0;
  for (let i = 0; i < game.length; i += 1) {
    for (let j = 0; j < game[i].length; j += 1) {
      if (game[i][j] === EXPLODED) {
        return true;
      }

      if (game[i][j] === FREE) {
        nFree += 1;
      }
    }
  }

  return nFree === 0;
}

function renderCell(c: number): string {
  if (c === EXPLODED) {
    return '*';
  }
  if (c >= 0) {
    return c.toString();
  }

  return '\xa0';
}

export function render(game: number[][]): string[][] {
  const r: string[][] = Array.of();
  for (let i = 0; i < game.length; i += 1) {
    r.push([]);
    for (let j = 0; j < game.length; j += 1) {
      r[i].push(renderCell(game[i][j]));
    }
  }

  return r;
}

function randInt(n: number): number {
  return Math.floor(Math.random() * n);
}


export function generateRandomGame(length: number, nBombs: number): number[][] {
  const game: number[][] = Array.of();
  for (let i = 0; i < length; i += 1) {
    game.push([]);
    for (let j = 0; j < length; j += 1) {
      game[i].push(FREE);
    }
  }

  const bombCoordinates = new Set<string>();
  while (bombCoordinates.size < nBombs) {
    const x = randInt(length);
    const y = randInt(length);
    bombCoordinates.add(`${x},${y}`);
  }

  bombCoordinates.forEach((bomb) => {
    const coordinates = bomb.split(',');
    const x = parseInt(coordinates[0], 10);
    const y = parseInt(coordinates[1], 10);
    game[x][y] = BOMB;
  });

  return game;
}
